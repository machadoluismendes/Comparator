
public class Pessoa {

	private String nome;
	private String sobrenome;
	private int idade;
	private String cargo;
	
	public Pessoa(String nome, String sobrenome, int idade, String cargo) {
		// TODO Auto-generated constructor stub
		
		super();
		
		setNome(nome);
		setSobrenome(sobrenome);
		setIdade(idade);
		setCargo(cargo);
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
	

}
